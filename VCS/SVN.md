# Subversion (SVN)

#### Create patch from commit

##### Cheat 1

    svn diff -c N path
    
> Where N is commit number and path is path to your working copy. [Source](https://stackoverflow.com/a/15031768)


##### Cheat 2

Creates a diff of all files from revision 123 to 124

    svn diff -r123:124 path/to/my_project > ~/my_project_changes_123_124.patch
    
> [Source](https://stackoverflow.com/a/25283101)